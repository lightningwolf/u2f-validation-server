# Copyright (c) 2013 Yubico AB
# All rights reserved.
#
#   Redistribution and use in source and binary forms, with or
#   without modification, are permitted provided that the following
#   conditions are met:
#
#    1. Redistributions of source code must retain the above copyright
#       notice, this list of conditions and the following disclaimer.
#    2. Redistributions in binary form must reproduce the above
#       copyright notice, this list of conditions and the following
#       disclaimer in the documentation and/or other materials provided
#       with the distribution.
#
# THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
# "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT
# LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS
# FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE
# COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT,
# INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING,
# BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES;
# LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
# CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT
# LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN
# ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
# POSSIBILITY OF SUCH DAMAGE.
import json
import unittest
from u2fval import create_app, db
from u2fval import exc
from u2fval.model import Client
from flask_migrate import Migrate, command
from six.moves.urllib.parse import quote
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import Encoding
from tests.soft_u2f_v2 import SoftU2FDevice, CERT


class RestApiTest(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        Migrate(self.app, db)
        self.migrate_config = self.app.extensions['migrate'].migrate.get_config(None, x_arg=None)
        command.upgrade(self.migrate_config, 'head', sql=False, tag=None)
        # db.create_all()
        db.session.add(
            Client('fooclient', 'https://example.com', ['https://example.com'])
        )
        db.session.commit()
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        db.session.remove()
        command.downgrade(self.migrate_config, 'base', sql=False, tag=None)
        # db.drop_all()
        self.app_context.pop()

    def test_call_without_client(self):
        resp = self.client.get('/')
        self.assertEqual(resp.status_code, 400)
        err = json.loads(resp.data.decode('utf8'))
        self.assertEqual(err['errorCode'], exc.BadInputException.code)

    def test_call_with_invalid_client(self):
        resp = self.client.get('/', environ_base={'REMOTE_USER': 'invalid'})
        self.assertEqual(resp.status_code, 404)
        err = json.loads(resp.data.decode('utf8'))
        self.assertEqual(err['errorCode'], exc.BadInputException.code)

    def test_get_trusted_facets(self):
        resp = json.loads(
            self.client.get(
                '/', environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8'))
        # print(resp)
        self.assertIn('https://example.com', resp['trustedFacets'][0]['ids'])

    def test_list_empty_devices(self):
        resp = json.loads(
            self.client.get(
                '/foouser', environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8'))
        self.assertEqual(resp, [])

    def test_begin_auth_without_devices(self):
        resp = self.client.get(
            '/foouser/sign',
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        self.assertEqual(resp.status_code, 400)
        err = json.loads(resp.data.decode('utf8'))
        self.assertEqual(err['errorCode'], exc.NoEligibleDevicesException.code)

    def test_register(self):
        device = SoftU2FDevice()
        self.do_register(device, {'foo': 'bar'})

    def test_sign(self):
        device = SoftU2FDevice()
        self.do_register(device, {'foo': 'bar', 'baz': 'one'})
        descriptor = self.do_sign(device, {'baz': 'two'})
        self.assertEqual(
            descriptor['properties'],
            {'foo': 'bar', 'baz': 'two'}
        )

    def test_get_properties(self):
        device = SoftU2FDevice()
        descriptor = self.do_register(device, {'foo': 'bar', 'baz': 'foo'})
        descriptor2 = json.loads(
            self.client.get(
                '/foouser/' + descriptor['handle'],
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        self.assertEqual(
            descriptor2['properties'],
            {'foo': 'bar', 'baz': 'foo'}
        )

    def test_update_properties(self):
        device = SoftU2FDevice()
        desc = self.do_register(
            device,
            {'foo': 'one', 'bar': 'one', 'baz': 'one'}
        )
        self.assertEqual({
            'foo': 'one',
            'bar': 'one',
            'baz': 'one'
        }, desc['properties'])

        desc2 = json.loads(self.client.post(
            '/foouser/' + desc['handle'],
            environ_base={'REMOTE_USER': 'fooclient'},
            data=json.dumps({'bar': 'two', 'baz': None})
        ).data.decode('utf8'))
        self.assertEqual({
            'foo': 'one',
            'bar': 'two'
        }, desc2['properties'])

        desc3 = json.loads(self.client.get(
            '/foouser/' + desc['handle'],
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
        self.assertEqual(desc2['properties'], desc3['properties'])

    def test_get_devices(self):
        self.do_register(SoftU2FDevice())
        self.do_register(SoftU2FDevice())
        self.do_register(SoftU2FDevice())

        resp = json.loads(
            self.client.get(
                '/foouser', environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        self.assertEqual(len(resp), 3)

    def test_get_device_descriptor_and_cert(self):
        desc = self.do_register(SoftU2FDevice())

        desc2 = json.loads(
            self.client.get(
                '/foouser/' + desc['handle'],
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )

        self.assertEqual(desc, desc2)

        cert = x509.load_pem_x509_certificate(
            self.client.get(
                '/foouser/' + desc['handle'] + '/certificate',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data, default_backend()
        )
        self.assertEqual(CERT, cert.public_bytes(Encoding.DER))

    def test_get_invalid_device(self):
        resp = self.client.get(
            '/foouser/' + ('ab' * 16),
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        self.assertEqual(resp.status_code, 404)

        self.do_register(SoftU2FDevice())
        resp = self.client.get(
            '/foouser/' + ('ab' * 16),
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        self.assertEqual(resp.status_code, 404)

        resp = self.client.get(
            '/foouser/InvalidHandle',
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        self.assertEqual(resp.status_code, 400)

    def test_delete_user(self):
        self.do_register(SoftU2FDevice())
        self.do_register(SoftU2FDevice())
        self.do_register(SoftU2FDevice())
        self.client.delete(
            '/foouser',
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        resp = json.loads(
            self.client.get(
                '/foouser', environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8'))
        self.assertEqual(resp, [])

    def test_delete_devices(self):
        d1 = self.do_register(SoftU2FDevice())
        d2 = self.do_register(SoftU2FDevice())
        d3 = self.do_register(SoftU2FDevice())

        self.client.delete(
            '/foouser/' + d2['handle'],
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        resp = json.loads(
            self.client.get(
                '/foouser',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        self.assertEqual(len(resp), 2)
        self.client.delete(
            '/foouser/' + d1['handle'],
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        resp = json.loads(
            self.client.get(
                '/foouser',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        self.assertEqual(len(resp), 1)
        self.assertEqual(d3, resp[0])
        self.client.delete(
            '/foouser/' + d3['handle'],
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        resp = json.loads(
            self.client.get(
                '/foouser',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        self.assertEqual(resp, [])

    def test_set_properties_during_register(self):
        device = SoftU2FDevice()
        reg_req = json.loads(self.client.get(
            '/foouser/register?properties=' + quote(json.dumps(
                {'foo': 'one', 'bar': 'one'})),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))

        reg_resp = device.register(
            'https://example.com', reg_req['appId'],
            reg_req['registerRequests'][0]).json

        desc = json.loads(self.client.post(
            '/foouser/register',
            data=json.dumps({
                'registerResponse': reg_resp,
                'properties': {'baz': 'two', 'bar': 'two'}
            }),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
        self.assertEqual(
            {'foo': 'one', 'bar': 'two', 'baz': 'two'},
            desc['properties']
        )

    def test_set_properties_during_sign(self):
        device = SoftU2FDevice()
        self.do_register(device, {'foo': 'one', 'bar': 'one', 'baz': 'one'})

        aut_req = json.loads(self.client.get(
            '/foouser/sign?properties=' + quote(json.dumps(
                {'bar': 'two', 'boo': 'two'})),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
        aut_resp = device.getAssertion(
            'https://example.com', aut_req['appId'],
            aut_req['challenge'],
            aut_req['registeredKeys'][0]).json
        desc = json.loads(self.client.post(
            '/foouser/sign',
            data=json.dumps({
                'signResponse': aut_resp,
                'properties': {'baz': 'three', 'boo': None}
            }),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
        self.assertEqual({
            'foo': 'one',
            'bar': 'two',
            'baz': 'three',
        }, desc['properties'])

    def test_register_and_sign_with_custom_challenge(self):
        device = SoftU2FDevice()
        reg_req = json.loads(self.client.get(
            '/foouser/register?challenge=ThisIsAChallenge',
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))

        self.assertEqual(
            reg_req['registerRequests'][0]['challenge'],
            'ThisIsAChallenge'
        )
        reg_resp = device.register(
            'https://example.com', reg_req['appId'],
            reg_req['registerRequests'][0]).json

        desc1 = json.loads(self.client.post(
            '/foouser/register',
            data=json.dumps({
                'registerResponse': reg_resp
            }),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))

        aut_req = json.loads(self.client.get(
            '/foouser/sign?challenge=ThisIsAChallenge',
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
        self.assertEqual(aut_req['challenge'], 'ThisIsAChallenge')
        aut_resp = device.getAssertion(
            'https://example.com', aut_req['appId'],
            aut_req['challenge'],
            aut_req['registeredKeys'][0]).json
        desc2 = json.loads(self.client.post(
            '/foouser/sign',
            data=json.dumps({
                'signResponse': aut_resp
            }),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
        self.assertEqual(desc1['handle'], desc2['handle'])

    def test_sign_with_handle_filtering(self):
        dev = SoftU2FDevice()
        h1 = self.do_register(dev)['handle']
        h2 = self.do_register(dev)['handle']
        self.do_register(dev)['handle']

        aut_req = json.loads(
            self.client.get(
                '/foouser/sign',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        self.assertEqual(len(aut_req['registeredKeys']), 3)
        self.assertEqual(len(aut_req['descriptors']), 3)

        aut_req = json.loads(
            self.client.get(
                '/foouser/sign?handle=' + h1,
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8'))
        self.assertEqual(len(aut_req['registeredKeys']), 1)
        self.assertEqual(aut_req['descriptors'][0]['handle'], h1)

        aut_req = json.loads(
            self.client.get(
                '/foouser/sign?handle=' + h1 + '&handle=' + h2,
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8'))
        self.assertEqual(len(aut_req['registeredKeys']), 2)
        self.assertIn(aut_req['descriptors'][0]['handle'], [h1, h2])
        self.assertIn(aut_req['descriptors'][1]['handle'], [h1, h2])

    def test_sign_with_invalid_handle(self):
        dev = SoftU2FDevice()
        self.do_register(dev)

        resp = self.client.get(
            '/foouser/sign?handle=foobar',
            environ_base={'REMOTE_USER': 'fooclient'}
        )
        self.assertEqual(resp.status_code, 400)

    def test_device_compromised_on_counter_error(self):
        dev = SoftU2FDevice()
        self.do_register(dev)
        self.do_sign(dev)
        self.do_sign(dev)
        self.do_sign(dev)
        dev.counter = 1

        aut_req = json.loads(
            self.client.get(
                '/foouser/sign',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        aut_resp = dev.getAssertion(
            'https://example.com', aut_req['appId'],
            aut_req['challenge'],
            aut_req['registeredKeys'][0]).json
        resp = self.client.post(
            '/foouser/sign',
            data=json.dumps({
                'signResponse': aut_resp
            }),
            environ_base={'REMOTE_USER': 'fooclient'}
        )

        self.assertEqual(400, resp.status_code)
        self.assertEqual(12, json.loads(resp.data.decode('utf8'))['errorCode'])

        resp = self.client.get('/foouser/sign',
                               environ_base={'REMOTE_USER': 'fooclient'})
        self.assertEqual(400, resp.status_code)
        self.assertEqual(11, json.loads(resp.data.decode('utf8'))['errorCode'])

    def do_register(self, device, properties=None):
        reg_req = json.loads(
            self.client.get(
                '/foouser/register',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        self.assertEqual(
            len(reg_req['registeredKeys']),
            len(reg_req['descriptors'])
        )

        reg_resp = device.register(
            'https://example.com', reg_req['appId'],
            reg_req['registerRequests'][0]).json

        if properties is None:
            properties = {}
        descriptor = json.loads(self.client.post(
            '/foouser/register',
            data=json.dumps({
                'registerResponse': reg_resp,
                'properties': properties
            }),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
        print(descriptor)
        self.assertEqual(descriptor['properties'], properties)
        return descriptor

    def do_sign(self, device, properties=None):
        aut_req = json.loads(
            self.client.get(
                '/foouser/sign',
                environ_base={'REMOTE_USER': 'fooclient'}
            ).data.decode('utf8')
        )
        aut_resp = device.getAssertion(
            'https://example.com', aut_req['appId'],
            aut_req['challenge'],
            aut_req['registeredKeys'][0]).json
        if properties is None:
            properties = {}
        return json.loads(self.client.post(
            '/foouser/sign',
            data=json.dumps({
                'signResponse': aut_resp,
                'properties': properties
            }),
            environ_base={'REMOTE_USER': 'fooclient'}
        ).data.decode('utf8'))
