import unittest
from flask import current_app
from u2fval import create_app, db
from flask_migrate import Migrate, command


class BasicsTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        Migrate(self.app, db)
        self.migrate_config = self.app.extensions['migrate'].migrate.get_config(None, x_arg=None)
        command.upgrade(self.migrate_config, 'head', sql=False, tag=None)
        # db.create_all()

    def tearDown(self):
        db.session.remove()
        command.downgrade(self.migrate_config, 'base', sql=False, tag=None)
        # db.drop_all()
        self.app_context.pop()

    def test_app_exists(self):
        self.assertFalse(current_app is None)

    def test_app_is_testing(self):
        self.assertTrue(current_app.config['TESTING'])
