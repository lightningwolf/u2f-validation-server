import re
import unittest
import pytest
from u2fval import create_app, db
from flask_migrate import Migrate, command


class FlaskClientTestCase(unittest.TestCase):
    def setUp(self):
        self.app = create_app('testing')
        self.app_context = self.app.app_context()
        self.app_context.push()
        Migrate(self.app, db)
        self.migrate_config = self.app.extensions['migrate'].migrate.get_config(None, x_arg=None)
        command.upgrade(self.migrate_config, 'head', sql=False, tag=None)
        # db.create_all()
        self.client = self.app.test_client(use_cookies=True)

    def tearDown(self):
        db.session.remove()
        command.downgrade(self.migrate_config, 'base', sql=False, tag=None)
        # db.drop_all()
        self.app_context.pop()

    def test_echo_page(self):
        response = self.client.get('/echo')
        self.assertEqual(response.status_code, 200)
        # self.assertTrue('Please enter your user information.' in response.get_data(as_text=True))

    def test_exc_test_page(self):
        with pytest.raises(Exception) as e_info:
            response = self.client.get('/exc_test')
            self.assertEqual(response.status_code, 200)
