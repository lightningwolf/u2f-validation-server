from __future__ import absolute_import

from u2fval.application import app, exc
from u2fval.model import db, Client, User
from u2fval.transactiondb import DBStore
from flask import g, request, jsonify
from werkzeug.contrib.cache import SimpleCache, MemcachedCache
from u2flib_server.utils import websafe_decode
from u2flib_server.u2f import (begin_registration, complete_registration,
                               begin_authentication, complete_authentication)
from u2fval.jsobjects import (RegisterRequestData, RegisterResponseData,
                              SignRequestData, SignResponseData)

from six.moves.urllib.parse import unquote
import json
import os
import re


if app.config['USE_MEMCACHED']:
    cache = MemcachedCache(app.config['MEMCACHED_SERVERS'])
else:
    cache = SimpleCache()


store = DBStore()


# Exception handling


@app.errorhandler(400)
def handle_bad_request(error):
    resp = jsonify({
        'errorCode': exc.BadInputException.code,
        'errorMessage': error.description
    })
    resp.status_code = error.code
    return resp


# Request handling




