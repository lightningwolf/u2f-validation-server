FROM registry.gitlab.com/ldath-core/docker/python-gosu:latest

ENV FLASK_APP u2fval/cli.py
ENV FLASK_CONFIG docker
ENV FLASK_ENV prod
ENV FLASK_DEBUG 0

# core requirements
RUN set -x \
    && apt-get update && apt-get install -y --no-install-recommends \
    build-essential libssl-dev libffi-dev \
    && rm -rf /var/lib/apt/lists/*

COPY requirements requirements
RUN pip install -r requirements/docker.txt

RUN set -x \
  && addgroup --gid 1100 u2fuser \
  && adduser --disabled-password --disabled-login --gecos '' --uid 1100 --gid 1100 --home /srv u2fuser

WORKDIR /srv

COPY u2fval u2fval
#COPY docs docs
COPY migrations migrations
COPY tests tests
COPY boot.sh cmd.sh requirements.txt uwsgi.ini /srv/

RUN chown -R u2fuser:u2fuser /srv

# run-time configuration
EXPOSE 5000 5001
ENTRYPOINT ["/srv/boot.sh"]
CMD ["/srv/cmd.sh"]
