from u2fval import app, exc
from u2fval.model import db, Client
from .soft_u2f_v2 import SoftU2FDevice, CERT
from six.moves.urllib.parse import quote
from cryptography import x509
from cryptography.hazmat.backends import default_backend
from cryptography.hazmat.primitives.serialization import Encoding
import unittest
import json


class RestApiTest(unittest.TestCase):

    def setUp(self):
        app.config['TESTING'] = True
        app.config['ALLOW_UNTRUSTED'] = True

        db.session.close()
        db.drop_all()
        db.create_all()
        db.session.add(Client('fooclient', 'https://example.com',
                              ['https://example.com']))
        db.session.commit()

        self.app = app.test_client()
