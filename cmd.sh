#!/bin/bash
set -e

if [[ "$FLASK_ENV" = 'DEV' ]]; then
    echo "Running Development Server"
    exec gosu u2fuser flask run --host=0.0.0.0 --port=5000
elif [[ "$FLASK_ENV" = 'TEST' ]]; then
    echo "Running Tests"
    exec gosu u2fuser flask test
else
    echo "Running Production Server"
#    exec gosu u2fuser uwsgi --http 0.0.0.0:5000 \
#        --wsgi-file "/srv/u2fval/application.py" \
#        --enable-threads --single-interpreter \
#        --lazy-apps \
#        --callable app --stats 0.0.0.0:5001
    exec gosu u2fuser uwsgi /srv/uwsgi.ini \
        --stats 0.0.0.0:5001
fi
