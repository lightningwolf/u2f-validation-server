import os

from dotenv import load_dotenv

from u2fval import create_app

dotenv_path = os.path.join(os.path.dirname(__file__), '../.env')
if os.path.exists(dotenv_path):
    load_dotenv(dotenv_path)

app = create_app(os.getenv('FLASK_CONFIG') or 'default')


class RemoteUserMiddleware(object):

    def __init__(self, app):
        self.app = app

    def __call__(self, environ, start_response):
        user = environ.pop('HTTP_X_PROXY_REMOTE_USER', None)
        # print("HTTP_X_PROXY_REMOTE_USER in the RemoteUserMiddleware: {}".format(user))
        environ['REMOTE_USER'] = user
        return self.app(environ, start_response)


app.wsgi_app = RemoteUserMiddleware(app.wsgi_app)
