from datetime import datetime
import os
from hashlib import sha256
import json

from flask import g, jsonify, request, current_app
from sqlalchemy.orm.exc import NoResultFound
from u2flib_server.attestation import MetadataProvider, create_resolver
from u2flib_server.u2f import (begin_registration, complete_registration,
                               begin_authentication, complete_authentication)

from . import cache
from .exc import BadInputException, NotFoundException, NoEligibleDevicesException, DeviceCompromisedException
from .jsobjects import (RegisterRequestData, RegisterResponseData,
                              SignRequestData, SignResponseData)
from .model import db, Client, User
from .transactiondb import DBStore

store = DBStore()


def get_client():
    client = getattr(g, 'client', None)
    if client is None:
        name = request.environ.get('REMOTE_USER')
        if name is None and current_app.debug and request.authorization:
            name = request.authorization.username
        if name is None:
            raise BadInputException('No client specified')
        try:
            g.client = client = Client.query.filter(Client.name == name).one()
        except NoResultFound:
            raise NotFoundException('Client not found')
    return client


def get_user(user_id):
    return get_client().users.filter(User.name == user_id).first()


def create_metadata_provider(location):
    if os.path.isfile(location) \
            or (os.path.isdir(location) and os.listdir(location)):
        resolver = create_resolver(location)
    else:
        resolver = None
    return MetadataProvider(resolver)


def get_attestation(cert):
    metadata = create_metadata_provider(current_app.config.get('METADATA'))
    key = sha256(cert).hexdigest()
    attestation = cache.get(key)
    if attestation is None:
        attestation = metadata.get_attestation(cert) or ''  # Cache "missing"
        cache.set(key, attestation, timeout=0)
    return attestation


def get_metadata(dev):
    key = 'cert_metadata/%d' % dev.certificate_id
    data = cache.get(key)
    if data is None:
        data = {}
        attestation = get_attestation(cert=dev.certificate.der)
        if attestation:
            if attestation.vendor_info:
                data['vendor'] = attestation.vendor_info
            if attestation.device_info:
                data['device'] = attestation.device_info
        cache.set(key, data, timeout=0)
    return data


def get_registered_key(dev, descriptor):
    key = json.loads(dev.bind_data)
    # Only keep appId if different from the "main" one.
    if key.get('appId') == get_client().app_id:
        del key['appId']
    # The 'version' field used to be missing in RegisteredKey.
    if 'version' not in key:
        key['version'] = 'U2F_V2'
    # Use transports from descriptor (which includes metadata)
    key['transports'] = descriptor['transports']

    return key


def register_request(user_id, challenge, properties):
    client = get_client()
    user = get_user(user_id)
    registered_keys = []
    descriptors = []
    if user is not None:
        for dev in user.devices.values():
            descriptor = dev.get_descriptor(get_metadata(dev))
            descriptors.append(descriptor)
            key = get_registered_key(dev, descriptor)
            registered_keys.append(key)
    request_data = begin_registration(
        client.app_id,
        registered_keys,
        challenge
    )
    request_data['properties'] = properties
    store.store(client.id, user_id, challenge, request_data.json)

    data = RegisterRequestData.wrap(request_data.data_for_client)
    data['descriptors'] = descriptors
    return data


def register_response(user_id, response_data):
    client = get_client()
    user = get_user(user_id)
    register_response = response_data.registerResponse
    challenge = register_response.clientData.challenge
    request_data = store.retrieve(client.id, user_id, challenge)
    if request_data is None:
        raise NotFoundException('Transaction not found')
    request_data = json.loads(request_data)
    registration, cert = complete_registration(
        request_data, register_response, client.valid_facets)
    attestation = get_attestation(cert)
    if not current_app.config['ALLOW_UNTRUSTED'] and not attestation.trusted:
        raise BadInputException('Device attestation not trusted')
    if user is None:
        current_app.logger.info('Creating user: %s/%s', client.name, user_id)
        user = User(user_id)
        client.users.append(user)
    transports = sum(t.value for t in attestation.transports or [])
    dev = user.add_device(registration.json, cert, transports)
    # Properties from the initial request have a lower precedence.
    dev.update_properties(request_data['properties'])
    dev.update_properties(response_data.properties)
    db.session.commit()
    current_app.logger.info('Registered device: %s/%s/%s', client.name, user_id,
                    dev.handle)
    return dev.get_descriptor(get_metadata(dev))


def sign_request(user_id, challenge, handles, properties):
    client = get_client()
    user = get_user(user_id)
    if user is None or len(user.devices) == 0:
        current_app.logger.info('User "%s" has no devices registered', user_id)
        raise NoEligibleDevicesException('No devices registered', [])

    registered_keys = []
    descriptors = []
    handle_map = {}

    if not handles:
        handles = user.devices.keys()

    for handle in handles:
        try:
            dev = user.devices[handle]
        except KeyError:
            raise BadInputException('Invalid device handle: ' + handle)
        if not dev.compromised:
            descriptor = dev.get_descriptor(get_metadata(dev))
            descriptors.append(descriptor)
            key = get_registered_key(dev, descriptor)
            registered_keys.append(key)
            handle_map[key['keyHandle']] = dev.handle

    if not registered_keys:
        raise NoEligibleDevicesException(
            'All devices compromised',
            [d.get_descriptor() for d in user.devices.values()]
        )

    request_data = begin_authentication(
        client.app_id,
        registered_keys,
        challenge
    )
    request_data['handleMap'] = handle_map
    request_data['properties'] = properties

    store.store(client.id, user_id, challenge, request_data.json)
    data = SignRequestData.wrap(request_data.data_for_client)
    data['descriptors'] = descriptors
    return data


def sign_response(user_id, response_data):
    client = get_client()
    user = get_user(user_id)
    sign_response = response_data.signResponse
    challenge = sign_response.clientData.challenge
    request_data = store.retrieve(client.id, user_id, challenge)
    if request_data is None:
        raise NotFoundException('Transaction not found')
    request_data = json.loads(request_data)
    device, counter, presence = complete_authentication(
        request_data, sign_response, client.valid_facets)
    dev = user.devices[request_data['handleMap'][device['keyHandle']]]
    if dev.compromised:
        raise DeviceCompromisedException('Device is compromised',
                                             dev.get_descriptor())
    if presence == 0:
        raise BadInputException('User presence byte not set')
    if counter > (dev.counter or -1):
        dev.counter = counter
        dev.authenticated_at = datetime.now()
        dev.update_properties(request_data['properties'])
        dev.update_properties(response_data.properties)
        db.session.commit()
        return dev.get_descriptor(get_metadata(dev))
    else:
        dev.compromised = True
        db.session.commit()
        raise DeviceCompromisedException('Device counter mismatch',
                                             dev.get_descriptor())
