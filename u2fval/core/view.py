from flask import abort, current_app, jsonify, request
from flask_sqlalchemy import get_debug_queries
import os
import re
from six.moves.urllib.parse import unquote
from sqlalchemy import func
import json
from u2flib_server.utils import websafe_decode

from . import core
from .. import exc
from ..helper import get_client, get_metadata, get_user, register_request, register_response, sign_request, sign_response
from ..jsobjects import RegisterResponseData, SignResponseData
from ..model import Client, db


@core.after_app_request
def after_request(response):
    for query in get_debug_queries():
        if query.duration >= current_app.config['APP_SLOW_DB_QUERY_TIME']:
            current_app.logger.warning(
                'Slow query: %s\nParameters: %s\nDuration: %fs\nContext: %s\n'
                % (query.statement, query.parameters, query.duration,
                   query.context))
    return response


@core.before_request
def before_request():
    db.engine.dispose()


# SELENIUM TESTS
# @core.route('/shutdown')
# def server_shutdown():
#     if not current_app.testing:
#         abort(404)
#     shutdown = request.environ.get('werkzeug.server.shutdown')
#     if not shutdown:
#         abort(500)
#     shutdown()
#     return 'Shutting down...'


@core.route('/echo', methods=['GET', 'POST'])
def index():
    clients = db.session.query(func.count(Client.id)).scalar()
    response = jsonify(
        {
            'status': 'success',
            'message': 'U2F validation server is UP; clients: {clients}.'.format(clients=clients)
        }
    )
    response.status_code = 200
    return response


@core.route('/exc_test', methods=['GET'])
def exc_test():
    raise Exception('Test exception')


@core.route('/')
def trusted_facets():
    client = get_client()
    return jsonify({
        'trustedFacets': [{
            'version': {'major': 1, 'minor': 0},
            'ids': client.valid_facets
        }]
    })


@core.route('/<user_id>', methods=['GET', 'DELETE'], strict_slashes=False)
def user(user_id):
    user_obj = get_user(user_id)
    if request.method == 'DELETE':
        if user_obj:
            current_app.logger.info(
                'Delete user: "%s/%s"', user_obj.client.name, user_obj.name
            )
            db.session.delete(user_obj)
            db.session.commit()
        return '', 204
    else:
        if user_obj is not None:
            descriptors = [d.get_descriptor(get_metadata(dev=d)) for d in user_obj.devices.values()]
        else:
            descriptors = []
        return jsonify(descriptors)


@core.route('/<user_id>/register', methods=['GET', 'POST'])
def register(user_id):
    if request.method == 'POST':
        # Response
        return jsonify(
            register_response(user_id, RegisterResponseData.wrap(request.get_json(force=True)))
        )
    else:
        # Request
        challenge = request.args.get('challenge', type=websafe_decode)
        if challenge is None:
            challenge = os.urandom(32)
        properties = request.args.get(
            'properties', {}, lambda x: json.loads(unquote(x))
        )

        return jsonify(register_request(user_id, challenge, properties))


@core.route('/<user_id>/sign', methods=['GET', 'POST'])
def sign(user_id):
    if request.method == 'POST':
        # Response
        return jsonify(sign_response(
            user_id, SignResponseData.wrap(request.get_json(force=True))))
    else:
        # Request
        challenge = request.args.get('challenge', type=websafe_decode)
        if challenge is None:
            challenge = os.urandom(32)
        properties = request.args.get('properties', {},
                                      lambda x: json.loads(unquote(x)))
        handles = request.args.getlist('handle')

        return jsonify(sign_request(user_id, challenge, handles, properties))


_HANDLE_PATTERN = re.compile(r'^[a-f0-9]{32}$')


@core.route('/<user_id>/<handle>', methods=['GET', 'POST', 'DELETE'])
def device(user_id, handle):
    if _HANDLE_PATTERN.match(handle) is None:
        raise exc.BadInputException('Invalid device handle: ' + handle)

    user = get_user(user_id)
    try:
        dev = user.devices[handle]
    except (AttributeError, KeyError):
        raise exc.NotFoundException('Device not found')

    if request.method == 'DELETE':
        if dev is not None:
            current_app.logger.info('Delete handle: %s/%s/%s', user.client.name,
                            user.name, handle)
            db.session.delete(dev)
            db.session.commit()
        return ('', 204)
    elif request.method == 'POST':
        if dev is None:
            raise exc.NotFoundException('Device not found')
        dev.update_properties(request.get_json(force=True))
        db.session.commit()
    else:
        if dev is None:
            raise exc.NotFoundException('Device not found')
    return jsonify(dev.get_descriptor(get_metadata(dev)))


@core.route('/<user_id>/<handle>/certificate')
def device_certificate(user_id, handle):
    user = get_user(user_id)
    if user is None:
        raise exc.NotFoundException('Device not found')
    try:
        dev = user.devices[handle]
    except KeyError:
        raise exc.BadInputException('Invalid device handle: ' + handle)
    return dev.certificate.get_pem()
