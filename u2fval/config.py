import os

basedir = os.path.abspath(os.path.dirname(__file__))


class Config(object):
    SECRET_KEY = os.environ.get('SECRET_KEY') or 'hard to guess string'

    # Slow query detection
    APP_SLOW_DB_QUERY_TIME = os.environ.get('APP_SLOW_DB_QUERY_TIME') or 0.5

    # Flask SSLify
    SSL_REDIRECT = os.environ.get('SSL_REDIRECT') or False

    # Sentry configuration
    SENTRY_CONFIG = {
        'dsn': os.environ.get('SENTRY_DSN', ''),
        'release': os.environ.get('APP_VERSION', 'latest'),
    }

    # Set to False to disable pretty-printing of JSON responses.
    # Note: XMLHttpRequests are never pretty-printed.
    JSONIFY_PRETTYPRINT_REGULAR = os.environ.get('JSONIFY_PRETTYPRINT_REGULAR') or True

    # Database configuration
    SQLALCHEMY_DATABASE_URI = os.environ.get('SQLALCHEMY_DATABASE_URI') or 'sqlite://'
    SQLALCHEMY_TRACK_MODIFICATIONS = os.environ.get('SQLALCHEMY_TRACK_MODIFICATIONS') or False
    SQLALCHEMY_RECORD_QUERIES = os.environ.get('SQLALCHEMY_RECORD_QUERIES') or False

    # If True, use memcached for storing registration and authentication requests
    # in progress, instead of persisting them to the database.
    USE_MEMCACHED = os.environ.get('USE_MEMCACHED') or False

    # If memcached is enabled, use these servers.
    # TODO: change to one const
    MEMCACHED_SERVERS_TO_SPLIT = os.environ.get('MEMCACHED_SERVERS_TO_SPLIT') or '127.0.0.1:11211'
    MEMCACHED_SERVERS = MEMCACHED_SERVERS_TO_SPLIT.split(",")

    # Add files containing trusted metadata JSON to the directory below.
    METADATA = os.environ.get('METADATA') or os.path.join(basedir, 'metadata')

    # Allow the use of untrusted (for which attestation cannot be verified using
    # the available trusted metadata) U2F devices.
    ALLOW_UNTRUSTED = os.environ.get('ALLOW_UNTRUSTED') or False

    @staticmethod
    def init_app(app):
        pass


class DevelopmentConfig(Config):
    DEBUG = True


class TestingConfig(Config):
    TESTING = True
    ALLOW_UNTRUSTED = True


class ProductionConfig(Config):
    @classmethod
    def init_app(cls, app):
        Config.init_app(app)


class DockerConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # log to stderr
        import logging
        from logging import StreamHandler
        file_handler = StreamHandler()
        file_handler.setLevel(logging.INFO)
        app.logger.addHandler(file_handler)


class UnixConfig(ProductionConfig):
    @classmethod
    def init_app(cls, app):
        ProductionConfig.init_app(app)

        # log to syslog
        import logging
        from logging.handlers import SysLogHandler
        syslog_handler = SysLogHandler()
        syslog_handler.setLevel(logging.INFO)
        app.logger.addHandler(syslog_handler)


config = {
    'development': DevelopmentConfig,
    'testing': TestingConfig,
    'production': ProductionConfig,
    'docker': DockerConfig,
    'unix': UnixConfig,

    'default': DevelopmentConfig
}
