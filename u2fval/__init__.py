from flask import Flask, jsonify
from flask_caching import Cache
from flask_sqlalchemy import SQLAlchemy
from raven.contrib.flask import Sentry
from u2fval.config import config
from u2fval.exc import BadInputException, U2fException
from werkzeug.exceptions import HTTPException

db = SQLAlchemy()
cache = Cache()


def create_app(config_name):
    app = Flask(__name__)

    configure_error_handler(app=app)

    app.config.from_object(config[config_name])
    config[config_name].init_app(app)

    Sentry(app)

    db.init_app(app)

    if app.config['USE_MEMCACHED']:
        cache_config = {'CACHE_TYPE': 'memcached', 'CACHE_MEMCACHED_SERVERS': app.config['MEMCACHED_SERVERS']}
    else:
        cache_config = {'CACHE_TYPE': 'simple'}
    cache.init_app(app, config=cache_config)

    if app.config['SSL_REDIRECT']:
        from flask_sslify import SSLify
        SSLify(app)

    from .core import core as core_blueprint
    app.register_blueprint(core_blueprint)

    return app


def make_json_error(ex):
    response = jsonify({'errorCode': ex.code, 'errorMessage': str(ex)})
    response.status_code = (ex.code if isinstance(ex, HTTPException) else 500)
    return response


def configure_error_handler(app):
    @app.errorhandler(U2fException)
    def handle_http_exception(error):
        resp = jsonify({
            'errorCode': error.code,
            'errorMessage': error.message
        })
        resp.status_code = error.status_code
        return resp

    @app.errorhandler(ValueError)
    def handle_value_error(error):
        resp = jsonify({
            'errorCode': BadInputException.code,
            'errorMessage': str(error)
        })
        resp.status_code = 400
        return resp

    @app.errorhandler(400)
    def handle_400(e):
        return make_json_error(e)

    @app.errorhandler(500)
    def handle_500(e):
        return make_json_error(e)
